package com.jogym.jogymapi.repository;

import com.jogym.jogymapi.entity.SeasonTicket;
import com.jogym.jogymapi.entity.StoreMember;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SeasonTicketRepository extends JpaRepository<SeasonTicket, Long> {
    Page<SeasonTicket> findAllByStoreMemberAndIsEnabledOrderByDateCreateDesc(StoreMember storeMember, Boolean isEnable, Pageable pageable);

    //Boolean existsBy


}
