package com.jogym.jogymapi.repository;

import com.jogym.jogymapi.entity.CalculateHistory;
import com.jogym.jogymapi.entity.StoreMember;
import com.jogym.jogymapi.enums.CalculateStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CalculateHistoryRepository extends JpaRepository<CalculateHistory, Long> {

    Optional<CalculateHistory> findByStoreMemberAndDateCreateYearAndDateCreateMonthAndCalculateStatus(StoreMember storeMember, Integer year, Integer month, CalculateStatus calculateStatus);

    Page<CalculateHistory> findAllByStoreMemberOrderByIdDesc(StoreMember storeMember, Pageable pageable);
}
