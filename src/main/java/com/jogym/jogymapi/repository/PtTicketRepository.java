package com.jogym.jogymapi.repository;

import com.jogym.jogymapi.entity.PtTicket;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PtTicketRepository extends JpaRepository<PtTicket, Long> {
    Page<PtTicket> findAllByIdGreaterThanEqualOrderByIdDesc (long id, Pageable pageable);
}
