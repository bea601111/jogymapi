package com.jogym.jogymapi.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
