package com.jogym.jogymapi.service.calculateHistory;

import com.jogym.jogymapi.entity.CalculateHistory;
import com.jogym.jogymapi.entity.StoreMember;
import com.jogym.jogymapi.enums.CalculateStatus;
import com.jogym.jogymapi.exception.CExistCompleteCalculateHistoryException;
import com.jogym.jogymapi.exception.CExistIngCalculateHistoryException;
import com.jogym.jogymapi.exception.CMissingDataException;
import com.jogym.jogymapi.model.calculatehistory.CalculateHistoryItem;
import com.jogym.jogymapi.model.calculatehistory.CalculateHistoryRequest;
import com.jogym.jogymapi.model.calculatehistory.CalculateHistoryResponse;
import com.jogym.jogymapi.model.calculatehistory.CalculateHistoryUpdateRequest;
import com.jogym.jogymapi.model.common.ListResult;
import com.jogym.jogymapi.repository.CalculateHistoryRepository;
import com.jogym.jogymapi.service.common.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CalculateHistoryService {
    private final CalculateHistoryRepository calculateHistoryRepository;

    public void setCalculateHistory(StoreMember storeMember, Double ptPrice, Double seasonPrice, CalculateHistoryRequest request){
        checkDupCalculateHistory(storeMember, request.getDateCreateYear(), request.getDateCreateMonth());
        CalculateHistory calculateHistory = new CalculateHistory.CalculateHistoryBuilder(storeMember, ptPrice + seasonPrice, request).build();
        calculateHistoryRepository.save(calculateHistory);
    }

    public ListResult<CalculateHistoryItem> getCalculateHistoryList(StoreMember storeMember, int page){
        Page<CalculateHistory> originList = calculateHistoryRepository.findAllByStoreMemberOrderByIdDesc(storeMember, ListConvertService.getPageable(page));

        List<CalculateHistoryItem> result = new LinkedList<>();

        originList.getContent().forEach(e -> {
            CalculateHistoryItem item = new CalculateHistoryItem.Builder(e).build();

            result.add(item);
        });
        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    public CalculateHistoryResponse getCalculateHistoryDetail(long historyId){
        CalculateHistory data = calculateHistoryRepository.findById(historyId).orElseThrow(CMissingDataException::new);

        return new CalculateHistoryResponse.Builder(data).build();
    }


    public void putCalculateHistory(long historyId, CalculateHistoryUpdateRequest request){
        CalculateHistory calculateHistory = calculateHistoryRepository.findById(historyId).orElseThrow(CMissingDataException::new);

        calculateHistory.putCalculateHistory(request);

        calculateHistoryRepository.save(calculateHistory);
    }

    private void checkDupCalculateHistory(StoreMember storeMember, int year, int month){
        Optional<CalculateHistory> isNewHistory = calculateHistoryRepository.findByStoreMemberAndDateCreateYearAndDateCreateMonthAndCalculateStatus(storeMember, year, month, CalculateStatus.ING);
        if (isNewHistory.isPresent()) throw new CExistIngCalculateHistoryException();
        Optional<CalculateHistory> existHistory = calculateHistoryRepository.findByStoreMemberAndDateCreateYearAndDateCreateMonthAndCalculateStatus(storeMember, year, month, CalculateStatus.COMPLETE);
        if (existHistory.isPresent()) throw new CExistCompleteCalculateHistoryException();
    }
}
