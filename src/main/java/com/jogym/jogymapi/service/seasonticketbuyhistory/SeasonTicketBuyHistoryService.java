package com.jogym.jogymapi.service.seasonticketbuyhistory;

import com.jogym.jogymapi.entity.*;
import com.jogym.jogymapi.enums.BuyStatus;
import com.jogym.jogymapi.exception.CAlreadyUsingSeasonTicketException;
import com.jogym.jogymapi.exception.CMissingDataException;
import com.jogym.jogymapi.exception.CNotExistMemberOfStoreMemberException;
import com.jogym.jogymapi.exception.CNotExistSeasonTicketOfStoreMemberException;
import com.jogym.jogymapi.model.common.ListResult;
import com.jogym.jogymapi.model.seasonticketbuyhistory.SeasonTicketBuyHistoryItem;
import com.jogym.jogymapi.repository.SeasonTicketBuyHistoryRepository;
import com.jogym.jogymapi.service.common.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SeasonTicketBuyHistoryService {
    private final SeasonTicketBuyHistoryRepository seasonTicketBuyHistoryRepository;

    public SeasonTicketBuyHistory getData(long id) {
        return seasonTicketBuyHistoryRepository.findById(id).orElseThrow(CMissingDataException::new );
    }

    public void setSeasonTicketBuyHistory(Member member, StoreMember storeMember, SeasonTicket seasonTicket){
        if (!storeMember.equals(member.getStoreMember())) throw new CNotExistMemberOfStoreMemberException(); // 회원이 체육관의 회원이 아닐 경우.
        if (!storeMember.equals(seasonTicket.getStoreMember())) throw new CNotExistSeasonTicketOfStoreMemberException(); // 해당 체육관에 맞지 않는 정기권일 경우.
        if (!isNewTicketOfMember(member)) throw new CAlreadyUsingSeasonTicketException(); //

        SeasonTicketBuyHistory seasonTicketBuyHistory = new SeasonTicketBuyHistory.SeasonTicketBuyHistoryBuilder(member, seasonTicket).build();

        seasonTicketBuyHistoryRepository.save(seasonTicketBuyHistory);
    }
    public void putHistoryDateLast(StoreMember storeMember, long historyId){
        SeasonTicketBuyHistory data = seasonTicketBuyHistoryRepository.findByMember_StoreMemberAndIdAndBuyStatus(storeMember, historyId, BuyStatus.VALID).orElseThrow(CMissingDataException::new);
        data.putDateLast();

        seasonTicketBuyHistoryRepository.save(data);

    }
    public void putSeasonTicketBuyHistoryBuyStatus(StoreMember storeMember, long historyId, BuyStatus buyStatus){
        SeasonTicketBuyHistory data = seasonTicketBuyHistoryRepository.findByMember_StoreMemberAndId(storeMember, historyId).orElseThrow(CMissingDataException::new);

        data.putBuyStatus(buyStatus);

        seasonTicketBuyHistoryRepository.save(data);
    }
    public ListResult<SeasonTicketBuyHistoryItem> getSeasonTicketBuyHistory(long memberId, StoreMember storeMember, int page){
        Page<SeasonTicketBuyHistory> originData = seasonTicketBuyHistoryRepository.findAllByMember_IdAndMember_StoreMemberOrderByIdDesc(memberId, storeMember, ListConvertService.getPageable(page));

        List<SeasonTicketBuyHistoryItem> result = new LinkedList<>();

        originData.getContent().forEach(e->{
            SeasonTicketBuyHistoryItem item = new SeasonTicketBuyHistoryItem.Builder(e).build();

            result.add(item);
        });
        return ListConvertService.settingResult(result, originData.getTotalElements(), originData.getTotalPages(), originData.getPageable().getPageNumber());
    }
    public Double totalPriceSeasonTicketByYearMonth(StoreMember storeMember, int year, int month){
        LocalDateTime startDateTime = LocalDateTime.of(year, month, getLastDay(year,month),0,0,0);
        LocalDateTime endDateTime = LocalDateTime.of(year, month, getLastDay(year, month),23,59,59);
        double totalPrice = 0D;
        List<SeasonTicketBuyHistory> originList = seasonTicketBuyHistoryRepository.findAllByMember_StoreMemberAndDateCreateBetween(storeMember, startDateTime, endDateTime);
        for (SeasonTicketBuyHistory item : originList) {
            totalPrice += (item.getSeasonTicket().getUnitPrice() * item.getSeasonTicket().getMaxMonth());
        }
        return totalPrice;
    }

    private boolean isNewTicketOfMember(Member member){
        long dupTicket = seasonTicketBuyHistoryRepository.countByMemberAndBuyStatus(member, BuyStatus.VALID);
        return dupTicket < 1;
        }

    private int getLastDay(Integer year, Integer month) {
        Calendar cal = Calendar.getInstance();
        cal.set(year, month - 1, 1);

        int lastDay = cal.getActualMaximum(Calendar.DATE);

        return lastDay;
    }
    }

