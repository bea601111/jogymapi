package com.jogym.jogymapi.service.boardReview;

import com.jogym.jogymapi.entity.BoardReview;
import com.jogym.jogymapi.entity.StoreMember;
import com.jogym.jogymapi.model.boardreview.BoardReviewRequest;
import com.jogym.jogymapi.repository.BoardReviewRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BoardReviewService {
    private final BoardReviewRepository boardReviewRepository;

    public void setBoardReview(StoreMember storeMember, BoardReviewRequest boardReviewRequest){
        BoardReview boardReview = new BoardReview.BoardReviewBuilder(storeMember, boardReviewRequest).build();

        boardReviewRepository.save(boardReview);
    }
}
