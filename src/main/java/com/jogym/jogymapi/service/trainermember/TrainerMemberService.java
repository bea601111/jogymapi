package com.jogym.jogymapi.service.trainermember;

import com.jogym.jogymapi.entity.Member;
import com.jogym.jogymapi.entity.StoreMember;
import com.jogym.jogymapi.entity.TrainerMember;
import com.jogym.jogymapi.exception.CMissingDataException;
import com.jogym.jogymapi.model.common.ListResult;
import com.jogym.jogymapi.model.member.MemberItem;
import com.jogym.jogymapi.model.member.MemberSearchRequest;
import com.jogym.jogymapi.model.storemember.StoreMemberItem;
import com.jogym.jogymapi.model.trainermember.*;
import com.jogym.jogymapi.repository.TrainerMemberRepository;
import com.jogym.jogymapi.service.common.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.aspectj.weaver.NewConstructorTypeMunger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TrainerMemberService {
    private final TrainerMemberRepository trainerMemberRepository;

    @PersistenceContext
    EntityManager entityManager;

    public TrainerMember getOriginTrainer(long trainerId) {
        return trainerMemberRepository.findById(trainerId).orElseThrow(CMissingDataException::new);
    }


    // Join이 걸린 StoreMember와 Model에 Request를 가져옴.
    // 직원 등록
    public void setTrainer(StoreMember storeMember, TrainerMemberRequest request) {
        TrainerMember trainerMember = new TrainerMember.TrainerMemberBuilder(storeMember, request).build();
        trainerMemberRepository.save(trainerMember);
    }

    // 트레이너의 숫자가 얼만큼의 될지 몰라 Page처리를 하였음.
    // Page를 처리 하지 않으면 많은 숫자의 트레이너를 한참 찾아야 함.
    // 직원 리스트 ( 페이징 )
    // 가맹점에 등록된 직원, 등록일기준 내림차순, isEnable == true 인 것만 찾음.
    public ListResult<TrainerMemberItem> getTrainerList(StoreMember storeMember, int page) {
        Page<TrainerMember> originList = trainerMemberRepository.findAllByStoreMemberAndIsEnabledOrderByDateCreateDesc(storeMember,true, ListConvertService.getPageable(page));
        List<TrainerMemberItem> result = new LinkedList<>();

        for (TrainerMember trainerMember : originList.getContent()) {
            result.add(new TrainerMemberItem.Builder(trainerMember).build());
        }
        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }// 직원 명 검색 리스트와 직원 상세정보가 필요.

    public ListResult<TrainerMemberItem> searchTrainerMembers(int pageNum, TrainerMemberSearchRequest searchRequest) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum, 10);

        Page<TrainerMember> trainerMembers = searchTrainerMember(pageRequest, searchRequest);

        ListResult<TrainerMemberItem> result = new ListResult<>();
        result.setTotalItemCount(trainerMembers.getTotalElements());
        result.setTotalPage(trainerMembers.getTotalPages() == 0 ? 1 : trainerMembers.getTotalPages());
        result.setCurrentPage(trainerMembers.getPageable().getPageNumber() + 1);

        List<TrainerMemberItem> list = new LinkedList<>();
        for (TrainerMember trainerMember : trainerMembers.getContent()) { // 원본을 가져옴
            list.add(new TrainerMemberItem.Builder(trainerMember).build());
            // 미리 생성한 동아줄에 Builder 안에 있는 내용을 생성
        }
        result.setList(list);

        return result;
    }

    private Page<TrainerMember> searchTrainerMember(Pageable pageable, TrainerMemberSearchRequest searchRequest) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<TrainerMember> criteriaQuery = criteriaBuilder.createQuery(TrainerMember.class);

        Root<TrainerMember> root = criteriaQuery.from(TrainerMember.class);

        List<Predicate> predicates = new LinkedList<>();
        if (searchRequest.getName() != null) predicates.add(criteriaBuilder.like(root.get("name"), "%" + searchRequest.getName() + "%"));
        if (searchRequest.getPhoneNumber() != null) predicates.add(criteriaBuilder.like(root.get("phoneNumber"), "%" + searchRequest.getPhoneNumber() + "%"));

        Predicate[] predArray = new Predicate[predicates.size()];
        predicates.toArray(predArray);
        criteriaQuery.where(predArray);

        TypedQuery<TrainerMember> query = entityManager.createQuery(criteriaQuery);

        int totalRows = query.getResultList().size();

        query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        query.setMaxResults(pageable.getPageSize());

        return new PageImpl<>(query.getResultList(), pageable, totalRows);
    }

    public TrainerMemberResponse getTrainer(long trainerId, StoreMember storeMember){
        TrainerMember trainerMember = trainerMemberRepository.findByIdAndStoreMember(trainerId,storeMember).orElseThrow(CMissingDataException::new);  // 데이터가 없습니다.
        if(!trainerMember.getIsEnabled()) throw new CMissingDataException(); // 이미 탈퇴한 직원
        return new TrainerMemberResponse.Builder(trainerMember).build();
    }


    // 트레이너의 정보를 수정 하기 위함.
    // 가맹점은 굳이 바꿀 필요가 없기에 가맹점을 뺐음.
    // id와 트레이너의 정보만 받아 수정을 하였음.
    // 직원 수정
    public void putTrainer(long trainerId, StoreMember storeMember, TrainerMemberUpdateRequest request) {
        TrainerMember originData = trainerMemberRepository.findById(trainerId).orElseThrow(CMissingDataException::new);
        if(!originData.getIsEnabled()) throw new CMissingDataException(); // 이미 탈퇴한 직원
        if(storeMember.getId() != originData.getStoreMember().getId()) throw new CMissingDataException(); // 가맹점이 다릅니다.
        originData.putTrainerMember(request);
        trainerMemberRepository.save(originData);
    }

    // 트레이너가 헬스장을 그만둘 경우를 대비하기 위함.
    // 트레이너가 헬스장을 그만두면 그 정보를 없애는게 아닌 활성화를 수정해 삭제를 함.
    // 헬스장에서 일할 때는 true로 하고 그만 둘 때는 false로 바꿈.
    // 그러나 이게 맞는지 모르겠음.
    // 직원 삭제
    public void putTrainerDelete(long trainerId, StoreMember storeMember) {
        TrainerMember originData = trainerMemberRepository.findById(trainerId).orElseThrow(CMissingDataException::new);
        if(!originData.getIsEnabled()) throw new CMissingDataException(); // 이미 탈퇴한 직원
        if(storeMember.getId() != originData.getStoreMember().getId()) throw new CMissingDataException(); // 가맹점이 다릅니다.
        originData.putTrainerMemberDelete();
        trainerMemberRepository.save(originData);
    }
}
