package com.jogym.jogymapi.exception;

public class CNotExistMemberOfStoreMemberException extends RuntimeException {
    public CNotExistMemberOfStoreMemberException(String msg, Throwable t) {
        super(msg, t);
    }

    public CNotExistMemberOfStoreMemberException(String msg) {
        super(msg);
    }

    public CNotExistMemberOfStoreMemberException() {
        super();
    }
}
