package com.jogym.jogymapi.controller.calculateHistory;

import com.jogym.jogymapi.entity.StoreMember;
import com.jogym.jogymapi.model.calculatehistory.CalculateHistoryItem;
import com.jogym.jogymapi.model.calculatehistory.CalculateHistoryRequest;
import com.jogym.jogymapi.model.calculatehistory.CalculateHistoryResponse;
import com.jogym.jogymapi.model.calculatehistory.CalculateHistoryUpdateRequest;
import com.jogym.jogymapi.model.common.CommonResult;
import com.jogym.jogymapi.model.common.ListResult;
import com.jogym.jogymapi.model.common.SingleResult;
import com.jogym.jogymapi.service.calculateHistory.CalculateHistoryService;
import com.jogym.jogymapi.service.common.ResponseService;
import com.jogym.jogymapi.service.ptTicketBuyHistory.PtTicketBuyHistoryService;
import com.jogym.jogymapi.service.seasonticketbuyhistory.SeasonTicketBuyHistoryService;
import com.jogym.jogymapi.service.storemember.StoreMemberService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.Response;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "정산 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/calculate")
public class CalculateHistoryController {
    private final PtTicketBuyHistoryService ptTicketBuyHistoryService;
    private final SeasonTicketBuyHistoryService seasonTicketBuyHistoryService;
    private final CalculateHistoryService calculateHistoryService;
    private final StoreMemberService storeMemberService;

    @ApiOperation(value = "정산내역 등록")
    @PostMapping("/data/store-member/{storeMemberId}")
    public CommonResult setCalculateHistory(@PathVariable long storeMemberId, @RequestBody @Valid CalculateHistoryRequest calculateHistoryRequest){
        StoreMember storeMember = storeMemberService.setStore(storeMemberId);
        double ptPrice = ptTicketBuyHistoryService.totalPricePtTicketByYearMonth(storeMember,calculateHistoryRequest.getDateCreateYear(), calculateHistoryRequest.getDateCreateMonth());
        double seasonPrice = seasonTicketBuyHistoryService.totalPriceSeasonTicketByYearMonth(storeMember, calculateHistoryRequest.getDateCreateYear(), calculateHistoryRequest.getDateCreateMonth());
        calculateHistoryService.setCalculateHistory(storeMember, ptPrice, seasonPrice, calculateHistoryRequest);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "정산내역 리스트")
    @GetMapping("/list/store-member/{storeMemberId}")
    public ListResult<CalculateHistoryItem> getCalculateHistoryList(@PathVariable long storeMemberId, @RequestParam(value = "page", required = false, defaultValue = "1") int page){
        StoreMember storeMember = storeMemberService.setStore(storeMemberId);
        return ResponseService.getListResult(calculateHistoryService.getCalculateHistoryList(storeMember, page), true);
    }

    @ApiOperation(value = "정산내역 상세보기")
    @GetMapping("/detail/history-id/{historyId}")
    public SingleResult<CalculateHistoryResponse> getCalculateHistoryDetail(@PathVariable long historyId){
        return ResponseService.getSingleResult(calculateHistoryService.getCalculateHistoryDetail(historyId));
    }

    @ApiOperation(value = "정산내역 수정")
    @PutMapping("/calculate-history/{calculateHistoryId}")
    public CommonResult putCalculateHistory(@PathVariable long calculateHistoryId, @RequestBody @Valid CalculateHistoryUpdateRequest request){
        calculateHistoryService.putCalculateHistory(calculateHistoryId, request);

        return ResponseService.getSuccessResult();
    }
}
