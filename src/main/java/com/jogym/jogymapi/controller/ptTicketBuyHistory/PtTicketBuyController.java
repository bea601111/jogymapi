package com.jogym.jogymapi.controller.ptTicketBuyHistory;

import com.jogym.jogymapi.entity.Member;
import com.jogym.jogymapi.entity.PtTicket;
import com.jogym.jogymapi.entity.StoreMember;
import com.jogym.jogymapi.enums.BuyStatus;
import com.jogym.jogymapi.model.common.CommonResult;
import com.jogym.jogymapi.model.common.ListResult;
import com.jogym.jogymapi.model.ptticketbuyhistory.PtTicketBuyHistoryItem;
import com.jogym.jogymapi.model.ptticketbuyhistory.PtTicketBuyHistoryRequest;
import com.jogym.jogymapi.service.common.ResponseService;
import com.jogym.jogymapi.service.member.MemberService;
import com.jogym.jogymapi.service.ptTicketBuyHistory.PtTicketBuyHistoryService;
import com.jogym.jogymapi.service.ptticket.PtTicketService;
import com.jogym.jogymapi.service.storemember.ProfileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "PT권 구매 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/pt-ticket-buy")
public class PtTicketBuyController {
    private final PtTicketBuyHistoryService ptTicketBuyHistoryService;
    private final ProfileService profileService;
    private final MemberService memberService;
    private final PtTicketService ptTicketService;

    @ApiOperation(value = "PT권 구매")
    @PostMapping("/data")
    public CommonResult setPtTicketBuyHistory(@RequestBody @Valid PtTicketBuyHistoryRequest request){
        StoreMember storeMember = profileService.getMemberData();
        Member member = memberService.getData(request.getMemberId());
        PtTicket ptTicket = ptTicketService.getPtTicketData(request.getPtTicketId());

        ptTicketBuyHistoryService.setPtTicketBuyHistory(member,storeMember,ptTicket,request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "구매내역 상태 변경")
    @PutMapping("/buy-status/buy-history-id/{buyHistoryId}")
    public CommonResult putHistoryBuyStatus(@PathVariable long buyHistoryId, @RequestParam("buy-status") BuyStatus buyStatus){
        StoreMember storeMember = profileService.getMemberData();
        ptTicketBuyHistoryService.putHistoryBuyStatus(storeMember, buyHistoryId, buyStatus);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "구매내역 리스트")
    @GetMapping("/list/member-id/{memberId}")
    public ListResult<PtTicketBuyHistoryItem> getSeasonTicketBuyHistory(@PathVariable long memberId, @RequestParam(value = "page",required = false, defaultValue = "1") int page){
        StoreMember storeMember = profileService.getMemberData();

        return ResponseService.getListResult(ptTicketBuyHistoryService.getSeasonTicketBuyHistory(memberId, storeMember,page), true);
    }
}
