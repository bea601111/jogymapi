package com.jogym.jogymapi.controller.boardReview;

import com.jogym.jogymapi.model.common.CommonResult;
import com.jogym.jogymapi.service.boardReview.BoardReviewService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "후기 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/board-review")
public class BoardReviewController {
    private final BoardReviewService boardReviewService;

/*    @ApiOperation(value = "후기 등록")
    @PostMapping("/data")
    public CommonResult setBoardReview(@PathVariable long storeMemberId){

    }*/
}
