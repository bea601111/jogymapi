package com.jogym.jogymapi.controller.storemember;

import com.jogym.jogymapi.model.common.CommonResult;
import com.jogym.jogymapi.model.common.ListResult;
import com.jogym.jogymapi.model.common.SingleResult;
import com.jogym.jogymapi.model.storemember.*;
import com.jogym.jogymapi.service.common.ResponseService;
import com.jogym.jogymapi.service.storemember.StoreMemberService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "가맹점 정보 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/store-member")
public class StoreMemberController {
    private final StoreMemberService storeMemberService;

    @ApiOperation(value = "가맹점 정보 등록")
    @PostMapping("/data")
    public CommonResult setStoreMember(@RequestBody @Valid StoreMemberCreateRequest request) {
        storeMemberService.setStoreMember(request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "가맹점 비밀번호 수정")
    @PutMapping("/password/store-member-id/{storeMemberId}")
    public CommonResult putPassword(@PathVariable long storeMemberId, @RequestBody @Valid StoreMemberUpdatePasswordRequest request) {
        storeMemberService.putPassword(storeMemberId, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "가맹점 정보 리스트")
    @GetMapping("/list")
    public ListResult<StoreMemberItem> getStoreMember(@RequestParam(value = "page", required = false, defaultValue = "1")int page) {
        return ResponseService.getListResult(storeMemberService.getStoreMember(page), true);
    }

    @GetMapping("/list/page-num/{pageNum}")
    public ListResult<StoreMemberItem> getList(
            @PathVariable int pageNum,
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "storeName", required = false) String storeName,
            @RequestParam(value = "username", required = false) String username
    ) {
        StoreMemberSearchRequest searchRequest = new StoreMemberSearchRequest();
        searchRequest.setName(name);
        searchRequest.setStoreName(storeName);
        searchRequest.setUsername(username);

        return ResponseService.getListResult(storeMemberService.searchStoreMembers(pageNum, searchRequest), true);
    }


    @ApiOperation(value = "가맹점 정보 리스트 상세보기")
    @GetMapping("/detail/id/{id}")
    public SingleResult<StoreMemberResponse> getStoreMemberDetail(@PathVariable long id) {
        return ResponseService.getSingleResult(storeMemberService.getStoreMemberDetail(id));
    }

    @ApiOperation(value = "가맹점 정보 수정")
    @PutMapping("/store-member-id/{storeMemberId}")
    public CommonResult putStoreMember(@PathVariable long storeMemberId, @RequestBody @Valid StoreMemberUpdateRequest request) {
        storeMemberService.putStoreMember(storeMemberId, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "가맹점 정보 삭제")
    @PutMapping("/status/store-member-id/{storeMemberId}")
    public CommonResult putStoreMemberDel(@PathVariable long storeMemberId) {
        storeMemberService.putStoreMemberDel(storeMemberId);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "id 중복확인")
    @GetMapping("/check/user-name")
    public SingleResult<CheckIsNewUsernameResponse> isNewUsername(@RequestParam("username") String userName){
        return ResponseService.getSingleResult(storeMemberService.isNewUsername(userName));
    }
    @ApiOperation(value = "사업자번호 중복확인")
    @GetMapping("/check/business-number")
    public SingleResult<CheckIsNewUsernameResponse> isNewBusinessNumber(@RequestParam("businessNumber") String businessNumber){
        return ResponseService.getSingleResult(storeMemberService.isNewBusinessNumber(businessNumber));
    }
}
