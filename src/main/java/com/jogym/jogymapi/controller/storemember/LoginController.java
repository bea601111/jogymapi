package com.jogym.jogymapi.controller.storemember;

import com.jogym.jogymapi.enums.MemberGroup;
import com.jogym.jogymapi.model.common.SingleResult;
import com.jogym.jogymapi.model.storemember.LoginRequest;
import com.jogym.jogymapi.model.storemember.LoginResponse;
import com.jogym.jogymapi.service.common.ResponseService;
import com.jogym.jogymapi.service.storemember.LoginService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Api(tags = "로그인")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member/login")
public class LoginController {
    private final LoginService loginService;

    @ApiOperation(value = "앱 - 관리자 로그인")
    @PostMapping("/app/admin")
    public SingleResult<LoginResponse> doLoginAdmin(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(MemberGroup.ROLE_ADMIN, loginRequest, "APP"));
    }

    @ApiOperation(value = "앱 - 일반유저 로그인")
    @PostMapping("/app/user")
    public SingleResult<LoginResponse> doLoginUser(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(MemberGroup.ROLE_STORE, loginRequest, "APP"));
    }
}
