package com.jogym.jogymapi.controller.member;

import com.jogym.jogymapi.entity.StoreMember;
import com.jogym.jogymapi.model.common.CommonResult;
import com.jogym.jogymapi.model.common.ListResult;
import com.jogym.jogymapi.model.member.MemberItem;
import com.jogym.jogymapi.model.member.MemberRequest;
import com.jogym.jogymapi.model.member.MemberUpdateRequest;
import com.jogym.jogymapi.service.common.ResponseService;
import com.jogym.jogymapi.service.member.MemberService;
import com.jogym.jogymapi.service.storemember.ProfileService;
import com.jogym.jogymapi.service.storemember.StoreMemberService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "회원 정보 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;
    private final ProfileService profileService;

    @ApiOperation(value = "회원 등록")
    @PostMapping("/data")
    public CommonResult setMember(@RequestBody @Valid MemberRequest request) {
        StoreMember storeMember = profileService.getMemberData();
        memberService.setMember(storeMember, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "회원 리스트")
    @GetMapping("/list")
    public ListResult<MemberItem> getMembers(@RequestParam(value = "page", required = false, defaultValue = "1") int page) {
        StoreMember storeMember = profileService.getMemberData();
        return ResponseService.getListResult(memberService.getMemberList(storeMember,page), true);
    }

    @ApiOperation(value = "회원 상세 정보")
    @GetMapping("/{memberId}")
    public CommonResult getMember(@PathVariable long memberId) {
        StoreMember storeMember = profileService.getMemberData();
        return ResponseService.getSingleResult(memberService.getMember(memberId, storeMember));
    }

    @ApiOperation(value = "회원 정보 수정")
    @PutMapping("/{memberId}")
    public CommonResult putMember(@PathVariable long memberId, @RequestBody @Valid MemberUpdateRequest request) {
        memberService.putMember(memberId, profileService.getMemberData(), request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "회원 정보 삭제")
    @PutMapping("/member-id/{memberId}")
    public CommonResult putMemberDelete(@PathVariable long memberId) {
        memberService.putMemberDelete(memberId, profileService.getMemberData());
        return ResponseService.getSuccessResult();
    }
}
