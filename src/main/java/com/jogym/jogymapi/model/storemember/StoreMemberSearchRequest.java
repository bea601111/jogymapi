package com.jogym.jogymapi.model.storemember;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StoreMemberSearchRequest {

    private String name;

    private String storeName;

    private String username;

}
