package com.jogym.jogymapi.model.trainermember;

import com.jogym.jogymapi.entity.TrainerMember;
import com.jogym.jogymapi.interfaces.CommonModelBuilder;
import com.jogym.jogymapi.lib.CommonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class TrainerMemberItem {
    @ApiModelProperty(notes = "시퀀스", required = true)
    @NotNull
    private Long id;

    @ApiModelProperty(notes = "등록일", required = true)
    @NotNull
    private String dateCreate;

    @ApiModelProperty(notes = "이름", required = true)
    @NotNull
    private String name;

    @ApiModelProperty(notes = "연락처", required = true)
    @NotNull
    private String phoneNumber;

    private TrainerMemberItem(Builder builder) {
        this.id = builder.id;
        this.dateCreate = builder.dateCreate;
        this.name = builder.name;
        this.phoneNumber = builder.phoneNumber;
    }

    public static class Builder implements CommonModelBuilder<TrainerMemberItem> {
        private final Long id;
        private final String dateCreate;
        private final String name;
        private final String phoneNumber;

        public Builder(TrainerMember trainerMember) {
            this.id = trainerMember.getId();
            this.dateCreate = CommonFormat.convertLocalDateTimeToString(trainerMember.getDateCreate());
            this.name = trainerMember.getName();
            this.phoneNumber = trainerMember.getPhoneNumber();
        }
        @Override
        public TrainerMemberItem build() {
            return new TrainerMemberItem(this);
        }
    }
}
