package com.jogym.jogymapi.model.trainermember;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TrainerMemberSearchRequest {

    private String name;

    private String phoneNumber;

}
