package com.jogym.jogymapi.model.trainermember;

import com.jogym.jogymapi.enums.Gender;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class TrainerMemberUpdateRequest {
    //( 이름, 연락처, 주소, 성별, 생년월일, 경력 및 자격 사항, 비고 )
    @ApiModelProperty(notes = "이름", required = true)
    @NotNull
    private String name;

    @ApiModelProperty(notes = "연락처", required = true)
    @NotNull
    private String phoneNumber;

    @ApiModelProperty(notes = "주소", required = true)
    @NotNull
    private String address;

    @ApiModelProperty(notes = "성별", required = true)
    @NotNull
    private Gender gender;

    @ApiModelProperty(notes = "생년월일", required = true)
    @NotNull
    private LocalDate dateBirth;

    @ApiModelProperty(notes = "경력 및 자격 사항", required = false)
    private String careerContent;

    @ApiModelProperty(notes = "비고", required = false)
    private String memo;
}
