package com.jogym.jogymapi.model.trainermember;

import com.jogym.jogymapi.entity.TrainerMember;
import com.jogym.jogymapi.interfaces.CommonModelBuilder;
import com.jogym.jogymapi.lib.CommonFormat;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class TrainerMemberResponse {
    //( 등록일, 이름, 연락처, 주소, 성별, 생년월일, 경력 및 자격 사항, 비고 )
    private String dateCreate;
    private String name;
    private String phoneNumber;
    private String address;
    private String gender;
    private LocalDate dateBirth;
    private String careerContent;
    private String memo;


    private TrainerMemberResponse(Builder builder){
        this.dateCreate = builder.dateCreate;
        this.name = builder.name;
        this.phoneNumber = builder.phoneNumber;
        this.address = builder.address;
        this.gender = builder.gender;
        this.dateBirth = builder.dateBirth;
        this.careerContent = builder.careerContent;
        this.memo = builder.memo;
    }



    public static class Builder implements CommonModelBuilder<TrainerMemberResponse>{

        private final String dateCreate;
        private final String name;
        private final String phoneNumber;
        private final String address;
        private final String gender;
        private final LocalDate dateBirth;
        private final String careerContent;
        private final String memo;

        public Builder(TrainerMember request){
            this.dateCreate = CommonFormat.convertLocalDateTimeToString(request.getDateCreate());
            this.name = request.getName();
            this.phoneNumber = request.getPhoneNumber();
            this.address = request.getAddress();
            this.gender = request.getGender().getName();
            this.dateBirth = request.getDateBirth();
            this.careerContent = request.getCareerContent();
            this.memo = request.getMemo();
        }

        @Override
        public TrainerMemberResponse build() {
            return new TrainerMemberResponse(this);
        }
    }


}
