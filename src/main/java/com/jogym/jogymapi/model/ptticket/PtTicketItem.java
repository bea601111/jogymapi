package com.jogym.jogymapi.model.ptticket;

import com.jogym.jogymapi.entity.PtTicket;
import com.jogym.jogymapi.entity.TrainerMember;
import com.jogym.jogymapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PtTicketItem {
    private Long id;

    private Long trainerMember;

    private String ticketName;

    private Integer maxCount;

    private Double unitPrice;

    private Double totalPrice;

    private PtTicketItem(Builder builder) {
        this.id = builder.id;
        this.trainerMember = builder.trainerMember;
        this.ticketName = builder.ticketName;
        this.maxCount = builder.maxCount;
        this.unitPrice = builder.unitPrice;
        this.totalPrice = builder.totalPrice;
    }

    public static class Builder implements CommonModelBuilder<PtTicketItem> {

        private final Long id;

        private final Long trainerMember;

        private final String ticketName;

        private final Integer maxCount;

        private final Double unitPrice;

        private final Double totalPrice;

        public Builder(PtTicket ptTicket) {
           this.id = ptTicket.getId();
            this.trainerMember = ptTicket.getTrainerMember().getId();
            this.ticketName = ptTicket.getTicketName();
            this.maxCount = ptTicket.getMaxCount();
            this.unitPrice = ptTicket.getUnitPrice();
            this.totalPrice = maxCount * unitPrice;
        }
        @Override
        public PtTicketItem build() {
            return new PtTicketItem(this);
        }
    }

}
