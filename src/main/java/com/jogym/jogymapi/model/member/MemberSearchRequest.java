package com.jogym.jogymapi.model.member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberSearchRequest {

    private String name;

    private String phoneNumber;
}
